/* istanbul ignore file */
const pool = require('../src/Infrastructures/database/postgres/pool');

const CommentsTableTestHelper = {
  async addComment({
    id = 'comment-123',
    content = 'comment content',
    date = new Date(),
    threadId = 'thread-123',
    userId = 'user-123',
  }) {
    const query = {
      text: `INSERT INTO comments (id, content, created_at, thread_id, user_id)
             VALUES ($1, $2, $3, $4, $5)
             RETURNING id`,
      values: [id, content, date, threadId, userId],
    };

    const result = await pool.query(query);

    return result.rows[0].id;
  },

  async findCommentsById(id) {
    const query = {
      text: 'SELECT * FROM comments WHERE id = $1',
      values: [id],
    };

    const result = await pool.query(query);

    return result.rows;
  },

  async cleanTable() {
    await pool.query('DELETE FROM comments WHERE 1=1');
  },
};

module.exports = CommentsTableTestHelper;
