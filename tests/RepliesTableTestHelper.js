/* istanbul ignore file */
const pool = require('../src/Infrastructures/database/postgres/pool');

const RepliesTableTestHelper = {
  async addReply({
    id = 'reply-123',
    content = 'reply content',
    date = new Date(),
    commentId = 'comment-123',
    userId = 'user-123',
  }) {
    const query = {
      text: `INSERT INTO replies (id, content, created_at, comment_id, user_id)
             VALUES ($1, $2, $3, $4, $5)
             RETURNING id`,
      values: [id, content, date, commentId, userId],
    };

    const result = await pool.query(query);

    return result.rows[0].id;
  },

  async cleanTable() {
    await pool.query('DELETE FROM replies WHERE 1=1');
  },
};

module.exports = RepliesTableTestHelper;
