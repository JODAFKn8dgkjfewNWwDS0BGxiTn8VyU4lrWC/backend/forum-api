const DomainErrorTranslator = require('../DomainErrorTranslator');
const InvariantError = require('../InvariantError');

describe('DomainErrorTranslator', () => {
  it('should translate error correctly', () => {
    const testCases = [
      {
        error: 'REGISTER_USER.NOT_CONTAIN_NEEDED_PROPERTY',
        message: 'tidak dapat membuat user baru karena properti yang dibutuhkan tidak ada',
      },
      {
        error: 'REGISTER_USER.NOT_MEET_DATA_TYPE_SPECIFICATION',
        message: 'tidak dapat membuat user baru karena tipe data tidak sesuai',
      },
      {
        error: 'REGISTER_USER.USERNAME_LIMIT_CHAR',
        message: 'tidak dapat membuat user baru karena karakter username melebihi batas limit',
      },
      {
        error: 'REGISTER_USER.USERNAME_CONTAIN_RESTRICTED_CHARACTER',
        message: 'tidak dapat membuat user baru karena username mengandung karakter terlarang',
      },
      {
        error: 'USER_LOGIN.NOT_CONTAIN_NEEDED_PROPERTY',
        message: 'harus mengirimkan username dan password',
      },
      {
        error: 'USER_LOGIN.NOT_MEET_DATA_TYPE_SPECIFICATION',
        message: 'username dan password harus string',
      },
      {
        error: 'REFRESH_AUTHENTICATION_USE_CASE.NOT_CONTAIN_REFRESH_TOKEN',
        message: 'harus mengirimkan token refresh',
      },
      {
        error: 'REFRESH_AUTHENTICATION_USE_CASE.PAYLOAD_NOT_MEET_DATA_TYPE_SPECIFICATION',
        message: 'refresh token harus string',
      },
      {
        error: 'DELETE_AUTHENTICATION_USE_CASE.NOT_CONTAIN_REFRESH_TOKEN',
        message: 'harus mengirimkan token refresh',
      },
      {
        error: 'DELETE_AUTHENTICATION_USE_CASE.PAYLOAD_NOT_MEET_DATA_TYPE_SPECIFICATION',
        message: 'refresh token harus string',
      },
      {
        error: 'NEW_THREAD.NOT_CONTAIN_NEEDED_PROPERTY',
        message: 'tidak dapat membuat utas baru karena properti yang dibutuhkan tidak ada',
      },
      {
        error: 'NEW_THREAD.NOT_MEET_DATA_TYPE_SPECIFICATION',
        message: 'tidak dapat membuat utas baru karena tipe data tidak sesuai',
      },
      {
        error: 'NEW_COMMENT.NOT_CONTAIN_NEEDED_PROPERTY',
        message: 'tidak dapat membuat komentar baru karena properti yang dibutuhkan tidak ada',
      },
      {
        error: 'NEW_COMMENT.NOT_MEET_DATA_TYPE_SPECIFICATION',
        message: 'tidak dapat membuat komentar baru karena tipe data tidak sesuai',
      }
    ];

    testCases.forEach((testCase) => {
      const error = new Error(testCase.error);
      const expectedError = new InvariantError(testCase.message);

      expect(DomainErrorTranslator.translate(error)).toStrictEqual(expectedError);
    });
  });

  it('should return original error when error message is not needed to translate', () => {
    // Arrange
    const error = new Error('some_error_message');

    // Action
    const translatedError = DomainErrorTranslator.translate(error);

    // Assert
    expect(translatedError).toStrictEqual(error);
  });
});
