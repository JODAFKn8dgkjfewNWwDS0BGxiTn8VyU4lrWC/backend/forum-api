const InvariantError = require('./InvariantError');

const DomainErrorTranslator = {
  translate(error) {
    const errorMessage = DomainErrorTranslator._directories[error.message];
    return errorMessage ? new InvariantError(errorMessage) : error;
  },
};

DomainErrorTranslator._directories = {
  'REGISTER_USER.NOT_CONTAIN_NEEDED_PROPERTY':
    'tidak dapat membuat user baru karena properti yang dibutuhkan tidak ada',
  'REGISTER_USER.NOT_MEET_DATA_TYPE_SPECIFICATION': 'tidak dapat membuat user baru karena tipe data tidak sesuai',
  'REGISTER_USER.USERNAME_LIMIT_CHAR': 'tidak dapat membuat user baru karena karakter username melebihi batas limit',
  'REGISTER_USER.USERNAME_CONTAIN_RESTRICTED_CHARACTER':
    'tidak dapat membuat user baru karena username mengandung karakter terlarang',
  'USER_LOGIN.NOT_CONTAIN_NEEDED_PROPERTY': 'harus mengirimkan username dan password',
  'USER_LOGIN.NOT_MEET_DATA_TYPE_SPECIFICATION': 'username dan password harus string',
  'REFRESH_AUTHENTICATION_USE_CASE.NOT_CONTAIN_REFRESH_TOKEN': 'harus mengirimkan token refresh',
  'REFRESH_AUTHENTICATION_USE_CASE.PAYLOAD_NOT_MEET_DATA_TYPE_SPECIFICATION': 'refresh token harus string',
  'DELETE_AUTHENTICATION_USE_CASE.NOT_CONTAIN_REFRESH_TOKEN': 'harus mengirimkan token refresh',
  'DELETE_AUTHENTICATION_USE_CASE.PAYLOAD_NOT_MEET_DATA_TYPE_SPECIFICATION': 'refresh token harus string',
  'NEW_THREAD.NOT_CONTAIN_NEEDED_PROPERTY': 'tidak dapat membuat utas baru karena properti yang dibutuhkan tidak ada',
  'NEW_THREAD.NOT_MEET_DATA_TYPE_SPECIFICATION': 'tidak dapat membuat utas baru karena tipe data tidak sesuai',
  'NEW_COMMENT.NOT_CONTAIN_NEEDED_PROPERTY':
    'tidak dapat membuat komentar baru karena properti yang dibutuhkan tidak ada',
  'NEW_COMMENT.NOT_MEET_DATA_TYPE_SPECIFICATION': 'tidak dapat membuat komentar baru karena tipe data tidak sesuai',
  'NEW_REPLY.NOT_CONTAIN_NEEDED_PROPERTY': 'tidak dapat membalas komentar karena properti yang dibutuhkan tidak ada',
  'NEW_REPLY.NOT_MEET_DATA_TYPE_SPECIFICATION': 'tidak dapat membalas komentar karena tipe data tidak sesuai',
};

module.exports = DomainErrorTranslator;
