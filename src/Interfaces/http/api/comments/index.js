const CommentsHandler = require('./handler');
const routes = require('./routes');

module.exports = {
  name: 'comments',
  async register(server, { container }) {
    const commentsHandler = new CommentsHandler(container);
    server.route(routes(commentsHandler));
  },
};
