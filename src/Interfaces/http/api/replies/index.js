const RepliesHandler = require('./handler');
const routes = require('./routes');

module.exports = {
  name: 'replies',
  async register(server, { container }) {
    const repliesHandler = new RepliesHandler(container);
    server.route(routes(repliesHandler));
  },
};
