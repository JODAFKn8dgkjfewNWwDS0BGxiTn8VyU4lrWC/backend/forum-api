const AuthenticationTokenManager = require('../AuthenticationTokenManager');

describe('AuthenticationTokenManager interface', () => {
  it('should throw error when invoke unimplemented method', async () => {
    // Arrange
    const tokenManager = new AuthenticationTokenManager();

    // Action & Assert
    const ERROR_MESSAGE = 'AUTHENTICATION_TOKEN_MANAGER.METHOD_NOT_IMPLEMENTED';
    await expect(tokenManager.createAccessToken('')).rejects.toThrowError(ERROR_MESSAGE);
    await expect(tokenManager.createRefreshToken('')).rejects.toThrowError(ERROR_MESSAGE);
    await expect(tokenManager.verifyRefreshToken('')).rejects.toThrowError(ERROR_MESSAGE);
    await expect(tokenManager.decodePayload('')).rejects.toThrowError(ERROR_MESSAGE);
  });
});
