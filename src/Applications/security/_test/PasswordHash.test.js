const EncryptionHelper = require('../PasswordHash');

describe('EncryptionHelper interface', () => {
  it('should throw error when invoke abstract behavior', async () => {
    // Arrange
    const encryptionHelper = new EncryptionHelper();

    // Action & Assert
    const ERROR_MESSAGE = 'PASSWORD_HASH.METHOD_NOT_IMPLEMENTED';
    await expect(encryptionHelper.hash('dummy_password')).rejects.toThrowError(ERROR_MESSAGE);
    await expect(encryptionHelper.comparePassword('plain', 'encrypted')).rejects.toThrowError(ERROR_MESSAGE);
  });
});
