const Comment = require('../../Domains/comments/entities/Comment');
const Reply = require('../../Domains/replies/entities/Reply');

class GetThreadDetailUseCase {
  constructor({ threadRepository, commentRepository, replyRepository }) {
    this._threadRepository = threadRepository;
    this._commentRepository = commentRepository;
    this._replyRepository = replyRepository;
  }

  async execute(useCasePayload) {
    const thread = await this._threadRepository.getThreadById(useCasePayload.threadId);
    const comments = await this._commentRepository.getCommentsByThreadId(useCasePayload.threadId);
    const replies = await this._replyRepository.getRepliesByThreadId(useCasePayload.threadId);

    return {
      ...thread,
      comments: comments.map((comment) => ({
        ...new Comment(comment),
        replies: replies.filter((reply) => reply.commentId === comment.id).map((reply) => new Reply(reply)),
      })),
    };
  }
}

module.exports = GetThreadDetailUseCase;
