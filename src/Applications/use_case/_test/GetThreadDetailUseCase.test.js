const ThreadRepository = require('../../../Domains/threads/ThreadRepository');
const CommentRepository = require('../../../Domains/comments/CommentRepository');
const ReplyRepository = require('../../../Domains/replies/ReplyRepository');
const Thread = require('../../../Domains/threads/entities/Thread');
const Comment = require('../../../Domains/comments/entities/Comment');
const Reply = require('../../../Domains/replies/entities/Reply');
const GetThreadDetailUseCase = require('../GetThreadDetailUseCase');

describe('GetThreadDetailUseCase', () => {
  it('should orchestrating the get thread detail action correctly', async () => {
    // Arrange
    const useCasePayload = {
      threadId: 'thread-123',
    };

    const threadPayload = {
      id: useCasePayload.threadId,
      title: 'thread title',
      body: 'thread body',
      date: new Date(),
      username: 'user-123',
    };

    const commentsPayload = [
      {
        id: 'comment-123',
        username: 'johndoe',
        date: new Date(),
        content: 'comment content',
        isDelete: false,
        threadId: useCasePayload.threadId,
      },
      {
        id: 'comment-124',
        username: 'dicoding',
        date: new Date(),
        content: 'comment content',
        isDelete: false,
        threadId: useCasePayload.threadId,
      }
    ];

    const repliesPayload = [
      {
        id: 'reply-123',
        username: 'dicoding',
        date: new Date(),
        content: 'reply content',
        isDelete: false,
        commentId: commentsPayload[0].id,
      },
      {
        id: 'reply-124',
        username: 'dicoding',
        date: new Date(),
        content: 'reply content',
        isDelete: true,
        commentId: commentsPayload[0].id,
      },
      {
        id: 'reply-125',
        username: 'johndoe',
        date: new Date(),
        content: 'reply content',
        isDelete: false,
        commentId: commentsPayload[1].id,
      }
    ];

    const mockThread = {
      id: threadPayload.id,
      title: threadPayload.title,
      body: threadPayload.body,
      date: threadPayload.date,
      username: threadPayload.username,
      comments: commentsPayload.map((comment) => ({
        ...new Comment(comment),
        replies: repliesPayload.filter((reply) => reply.commentId === comment.id).map((reply) => new Reply(reply)),
      })),
    };

    /** creating dependency of use case */
    const mockThreadRepository = new ThreadRepository();
    const mockCommentRepository = new CommentRepository();
    const mockReplyRepository = new ReplyRepository();

    /** mocking needed function */
    mockThreadRepository.getThreadById = jest.fn().mockResolvedValue(new Thread(threadPayload));
    mockCommentRepository.getCommentsByThreadId = jest.fn().mockResolvedValue(commentsPayload);
    mockReplyRepository.getRepliesByThreadId = jest.fn().mockResolvedValue(repliesPayload);

    /** creating use case instance */
    const getThreadDetailUseCase = new GetThreadDetailUseCase({
      threadRepository: mockThreadRepository,
      commentRepository: mockCommentRepository,
      replyRepository: mockReplyRepository,
    });

    // Action
    const thread = await getThreadDetailUseCase.execute(useCasePayload);

    // Assert
    expect(thread).toStrictEqual(mockThread);
    expect(mockThreadRepository.getThreadById).toBeCalledWith(useCasePayload.threadId);
    expect(mockCommentRepository.getCommentsByThreadId).toBeCalledWith(useCasePayload.threadId);
    expect(mockReplyRepository.getRepliesByThreadId).toBeCalledWith(useCasePayload.threadId);
  });
});
