const ThreadRepository = require('../../Domains/threads/ThreadRepository');
const AddedThread = require('../../Domains/threads/entities/AddedThread');
const NotFoundError = require('../../Commons/exceptions/NotFoundError');
const Thread = require('../../Domains/threads/entities/Thread');

class ThreadRepositoryPostgres extends ThreadRepository {
  constructor(pool, idGenerator) {
    super();
    this._pool = pool;
    this._idGenerator = idGenerator;
  }

  async addThread(newThread) {
    const { title, body, owner } = newThread;
    const id = `thread-${this._idGenerator()}`;

    const query = {
      text: `INSERT INTO threads (id, title, body, user_id)
             VALUES ($1, $2, $3, $4)
             RETURNING id, title, user_id AS owner`,
      values: [id, title, body, owner],
    };

    const result = await this._pool.query(query);

    return new AddedThread(result.rows[0]);
  }

  async verifyThreadExist(id) {
    const query = {
      text: 'SELECT id FROM threads WHERE id = $1',
      values: [id],
    };

    const result = await this._pool.query(query);

    if (!result.rowCount) {
      throw new NotFoundError('utas tidak ditemukan');
    }
  }

  async getThreadById(id) {
    const query = {
      text: `SELECT t.id, title, body, created_at AS date, username
             FROM threads AS t
             INNER JOIN users AS u ON t.user_id = u.id
             WHERE t.id = $1`,
      values: [id],
    };

    const result = await this._pool.query(query);

    if (!result.rowCount) {
      throw new NotFoundError('utas tidak ditemukan');
    }

    return new Thread(result.rows[0]);
  }
}

module.exports = ThreadRepositoryPostgres;
