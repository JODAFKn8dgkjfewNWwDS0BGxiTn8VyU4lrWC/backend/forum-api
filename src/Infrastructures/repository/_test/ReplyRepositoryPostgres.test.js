const RepliesTableTestHelper = require('../../../../tests/RepliesTableTestHelper');
const CommentsTableTestHelper = require('../../../../tests/CommentsTableTestHelper');
const ThreadsTableTestHelper = require('../../../../tests/ThreadsTableTestHelper');
const UsersTableTestHelper = require('../../../../tests/UsersTableTestHelper');
const pool = require('../../database/postgres/pool');
const ReplyRepositoryPostgres = require('../ReplyRepositoryPostgres');
const AddedReply = require('../../../Domains/replies/entities/AddedReply');
const NotFoundError = require('../../../Commons/exceptions/NotFoundError');
const AuthorizationError = require('../../../Commons/exceptions/AuthorizationError');

describe('ReplyRepositoryPostgres', () => {
  afterEach(async () => {
    await RepliesTableTestHelper.cleanTable();
    await CommentsTableTestHelper.cleanTable();
    await ThreadsTableTestHelper.cleanTable();
    await UsersTableTestHelper.cleanTable();
  });

  afterAll(async () => {
    await pool.end();
  });

  describe('addReply function', () => {
    it('should return created reply correctly', async () => {
      // Arrange
      const ownerId = await UsersTableTestHelper.addUser({ username: 'dicoding' });
      const threadId = await ThreadsTableTestHelper.addThread({ userId: ownerId });
      const commentId = await CommentsTableTestHelper.addComment({ owner: ownerId, threadId });
      const newReply = {
        content: 'reply content',
        owner: ownerId,
        commentId,
      };
      const fakeIdGenerator = () => '123';
      const replyRepositoryPostgres = new ReplyRepositoryPostgres(pool, fakeIdGenerator);

      // Action
      const addedReply = await replyRepositoryPostgres.addReply(newReply);

      // Assert
      expect(addedReply).toStrictEqual(
        new AddedReply({
          id: `reply-${fakeIdGenerator()}`,
          content: newReply.content,
          owner: newReply.owner,
        })
      );
    });
  });

  describe('getRepliesByThreadId function', () => {
    it('should return replies correctly', async () => {
      // Arrange
      const replyPayload = {
        id: 'reply-123',
        content: 'reply content',
        date: new Date(),
        username: 'dicoding',
      };
      const ownerId = await UsersTableTestHelper.addUser({ username: replyPayload.username });
      const threadId = await ThreadsTableTestHelper.addThread({ userId: ownerId });
      const commentId = await CommentsTableTestHelper.addComment({ owner: ownerId, threadId });
      await RepliesTableTestHelper.addReply({
        id: replyPayload.id,
        content: replyPayload.content,
        date: replyPayload.date,
        commentId,
        owner: ownerId,
      });
      const replyRepositoryPostgres = new ReplyRepositoryPostgres(pool, {});

      // Action
      const replies = await replyRepositoryPostgres.getRepliesByThreadId(threadId);

      // Assert
      expect(replies).toHaveLength(1);
      expect(replies[0].id).toEqual(replyPayload.id);
      expect(replies[0].content).toEqual(replyPayload.content);
      expect(replies[0].date).toEqual(replyPayload.date);
      expect(replies[0].username).toEqual(replyPayload.username);
    });
  });

  describe('verifyReplyOwner function', () => {
    it('should throw an error when the comment reply is not found', async () => {
      // Arrange
      const replyRepositoryPostgres = new ReplyRepositoryPostgres(pool, {});
      const nonExistentReplyId = 'reply-xxx';

      // Action & Assert
      await expect(replyRepositoryPostgres.verifyReplyOwner(nonExistentReplyId, '')).rejects.toThrow(NotFoundError);
    });

    it('should throw an error when the comment reply owner is not matching', async () => {
      // Arrange
      const ownerId = await UsersTableTestHelper.addUser({ username: 'dicoding' });
      const threadId = await ThreadsTableTestHelper.addThread({ userId: ownerId });
      const commentId = await CommentsTableTestHelper.addComment({ threadId, userId: ownerId });
      const replyId = await RepliesTableTestHelper.addReply({ commentId, userId: ownerId });
      const replyRepositoryPostgres = new ReplyRepositoryPostgres(pool, {});
      const nonMatchingOwnerId = 'user-xxx';

      // Action & Assert
      await expect(replyRepositoryPostgres.verifyReplyOwner(replyId, nonMatchingOwnerId)).rejects.toThrow(
        AuthorizationError
      );
    });
  });

  describe('deleteReplyById function', () => {
    it('should throw an error when the comment reply is not found', async () => {
      // Arrange
      const replyRepositoryPostgres = new ReplyRepositoryPostgres(pool, {});
      const nonExistentReplyId = 'reply-xxx';

      // Action & Assert
      await expect(replyRepositoryPostgres.verifyReplyOwner(nonExistentReplyId, '')).rejects.toThrow(NotFoundError);
    });

    it('should delete comment reply', async () => {
      // Arrange
      const ownerId = await UsersTableTestHelper.addUser({ username: 'dicoding' });
      const threadId = await ThreadsTableTestHelper.addThread({ userId: ownerId });
      const commentId = await CommentsTableTestHelper.addComment({ threadId, userId: ownerId });
      const replyId = await RepliesTableTestHelper.addReply({ commentId, userId: ownerId });
      const replyRepositoryPostgres = new ReplyRepositoryPostgres(pool, {});

      // Action & Assert
      await expect(replyRepositoryPostgres.deleteReplyById(replyId)).resolves.not.toThrow();
    });
  });
});
