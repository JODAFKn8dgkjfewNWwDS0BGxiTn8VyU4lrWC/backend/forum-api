const pool = require('../../database/postgres/pool');
const CommentsTableTestHelper = require('../../../../tests/CommentsTableTestHelper');
const ThreadsTableTestHelper = require('../../../../tests/ThreadsTableTestHelper');
const UsersTableTestHelper = require('../../../../tests/UsersTableTestHelper');
const CommentRepositoryPostgres = require('../CommentRepositoryPostgres');
const AuthorizationError = require('../../../Commons/exceptions/AuthorizationError');
const NotFoundError = require('../../../Commons/exceptions/NotFoundError');
const AddedComment = require('../../../Domains/comments/entities/AddedComment');

describe('CommentRepositoryPostgres', () => {
  afterEach(async () => {
    await CommentsTableTestHelper.cleanTable();
    await ThreadsTableTestHelper.cleanTable();
    await UsersTableTestHelper.cleanTable();
  });

  afterAll(async () => {
    await pool.end();
  });

  describe('addComment function', () => {
    it('should return created thread comment correctly', async () => {
      // Arrange
      const ownerId = await UsersTableTestHelper.addUser({ username: 'dicoding' });
      const threadId = await ThreadsTableTestHelper.addThread({ userId: ownerId });
      const newComment = {
        content: 'comment content',
        owner: ownerId,
        threadId,
      };
      const fakeIdGenerator = () => '123';
      const commentRepositoryPostgres = new CommentRepositoryPostgres(pool, fakeIdGenerator);

      // Action
      const addedComment = await commentRepositoryPostgres.addComment(newComment);

      // Assert
      expect(addedComment).toStrictEqual(
        new AddedComment({
          id: `comment-${fakeIdGenerator()}`,
          content: newComment.content,
          owner: newComment.owner,
        })
      );
    });

    it('should create new thread comment', async () => {
      // Arrange
      const ownerId = await UsersTableTestHelper.addUser({ username: 'dicoding' });
      const threadId = await ThreadsTableTestHelper.addThread({ userId: ownerId });
      const newComment = {
        content: 'comment content',
        owner: ownerId,
        threadId,
      };
      const fakeIdGenerator = () => '123';
      const commentRepositoryPostgres = new CommentRepositoryPostgres(pool, fakeIdGenerator);

      // Action
      await commentRepositoryPostgres.addComment(newComment);

      // Assert
      const comments = await CommentsTableTestHelper.findCommentsById('comment-123');
      expect(comments).toHaveLength(1);
    });
  });

  describe('verifyCommentOwner function', () => {
    it('should throw an error when the comment is not found', async () => {
      // Arrange
      const commentRepositoryPostgres = new CommentRepositoryPostgres(pool, {});
      const nonExistentCommentId = 'comment-xxx';

      // Action & Assert
      await expect(commentRepositoryPostgres.verifyCommentOwner(nonExistentCommentId, '')).rejects.toThrow(
        NotFoundError
      );
    });

    it('should throw an error when the comment owner is not matching', async () => {
      // Arrange
      const ownerId = await UsersTableTestHelper.addUser({ username: 'dicoding' });
      const threadId = await ThreadsTableTestHelper.addThread({ userId: ownerId });
      const commentId = await CommentsTableTestHelper.addComment({ threadId, userId: ownerId });
      const commentRepositoryPostgres = new CommentRepositoryPostgres(pool, {});
      const nonMatchingOwnerId = 'user-xxx';

      // Action & Assert
      await expect(commentRepositoryPostgres.verifyCommentOwner(commentId, nonMatchingOwnerId)).rejects.toThrow(
        AuthorizationError
      );
    });
  });

  describe('deleteCommentById function', () => {
    it('should throw an error when the comment is not found', async () => {
      // Arrange
      const commentRepositoryPostgres = new CommentRepositoryPostgres(pool, {});
      const nonExistentCommentId = 'comment-xxx';

      // Action & Assert
      await expect(commentRepositoryPostgres.verifyCommentOwner(nonExistentCommentId, '')).rejects.toThrow(
        NotFoundError
      );
    });

    it('should delete comment', async () => {
      // Arrange
      const ownerId = await UsersTableTestHelper.addUser({ username: 'dicoding' });
      const threadId = await ThreadsTableTestHelper.addThread({ userId: ownerId });
      const commentId = await CommentsTableTestHelper.addComment({ threadId, userId: ownerId });
      const commentRepositoryPostgres = new CommentRepositoryPostgres(pool, {});

      // Action & Assert
      await expect(commentRepositoryPostgres.deleteCommentById(commentId)).resolves.not.toThrow();
    });
  });

  describe('getCommentsByThreadId function', () => {
    it('should return comments correctly', async () => {
      // Arrange
      const commentPayload = {
        id: 'comment-123',
        content: 'comment content',
        date: new Date(),
        username: 'dicoding',
      };
      const ownerId = await UsersTableTestHelper.addUser({ username: commentPayload.username });
      const threadId = await ThreadsTableTestHelper.addThread({ userId: ownerId });
      const commentId = await CommentsTableTestHelper.addComment({
        id: commentPayload.id,
        content: commentPayload.content,
        date: commentPayload.date,
        threadId,
        userId: ownerId,
      });
      const commentRepositoryPostgres = new CommentRepositoryPostgres(pool, {});

      // Action
      const comments = await commentRepositoryPostgres.getCommentsByThreadId(threadId);

      // Assert
      expect(comments).toHaveLength(1);
      expect(comments[0].id).toStrictEqual(commentId);
      expect(comments[0].content).toStrictEqual(commentPayload.content);
      expect(comments[0].date).toStrictEqual(commentPayload.date);
      expect(comments[0].username).toStrictEqual(commentPayload.username);
    });
  });

  describe('verifyCommentExist function', () => {
    it("should throw an error if the comment doesn't exist", async () => {
      // Arrange
      const nonExistentCommentId = 'comment-xxx';
      const commentRepositoryPostgres = new CommentRepositoryPostgres(pool, {});

      // Action & Assert
      await expect(commentRepositoryPostgres.verifyCommentExist(nonExistentCommentId)).rejects.toThrow(NotFoundError);
    });
  });
});
