const ThreadsTableTestHelper = require('../../../../tests/ThreadsTableTestHelper');
const pool = require('../../database/postgres/pool');
const UsersTableTestHelper = require('../../../../tests/UsersTableTestHelper');
const NewThread = require('../../../Domains/threads/entities/NewThread');
const ThreadRepositoryPostgres = require('../ThreadRepositoryPostgres');
const NotFoundError = require('../../../Commons/exceptions/NotFoundError');
const AddedThread = require('../../../Domains/threads/entities/AddedThread');

describe('ThreadRepositoryPostgres', () => {
  afterEach(async () => {
    await ThreadsTableTestHelper.cleanTable();
    await UsersTableTestHelper.cleanTable();
  });

  afterAll(async () => {
    await pool.end();
  });

  describe('addThread function', () => {
    it('should return created thread correctly', async () => {
      // Arrange
      const ownerId = await UsersTableTestHelper.addUser({ username: 'dicoding' });
      const newThread = new NewThread({
        title: 'thread title',
        body: 'thread body',
        owner: ownerId,
      });
      const fakeIdGenerator = () => '123';
      const threadRepositoryPostgres = new ThreadRepositoryPostgres(pool, fakeIdGenerator);

      // Action
      const addedThread = await threadRepositoryPostgres.addThread(newThread);

      // Assert
      expect(addedThread).toStrictEqual(
        new AddedThread({
          id: `thread-${fakeIdGenerator()}`,
          title: newThread.title,
          owner: newThread.owner,
        })
      );
    });

    it('should create new thread', async () => {
      // Arrange
      const ownerId = await UsersTableTestHelper.addUser({ username: 'dicoding' });
      const newThread = new NewThread({
        title: 'thread title',
        body: 'thread body',
        owner: ownerId,
      });
      const fakeIdGenerator = () => '123';
      const threadRepositoryPostgres = new ThreadRepositoryPostgres(pool, fakeIdGenerator);

      // Action
      await threadRepositoryPostgres.addThread(newThread);

      // Assert
      const threads = await ThreadsTableTestHelper.findThreadsById('thread-123');
      expect(threads).toHaveLength(1);
    });
  });

  describe('verifyThreadExist function', () => {
    it("should throw an error if the thread doesn't exist", async () => {
      // Arrange
      const nonExistentThreadId = 'thread-xxx';
      const threadRepositoryPostgres = new ThreadRepositoryPostgres(pool, {});

      // Action & Assert
      await expect(threadRepositoryPostgres.verifyThreadExist(nonExistentThreadId)).rejects.toThrow(NotFoundError);
    });
  });

  describe('getThreadById function', () => {
    it('should return thread detail', async () => {
      // Arrange
      const threadPayload = {
        id: 'thread-123',
        title: 'thread title',
        body: 'thread body',
        date: new Date(),
        username: 'dicoding',
      };
      const ownerId = await UsersTableTestHelper.addUser({ username: threadPayload.username });
      const threadId = await ThreadsTableTestHelper.addThread({
        id: threadPayload.id,
        title: threadPayload.title,
        body: threadPayload.body,
        date: threadPayload.date,
        owner: ownerId,
      });
      const threadRepositoryPostgres = new ThreadRepositoryPostgres(pool, {});

      // Action
      const thread = await threadRepositoryPostgres.getThreadById(threadId);

      // Assert
      expect(thread.id).toEqual(threadPayload.id);
      expect(thread.title).toEqual(threadPayload.title);
      expect(thread.body).toEqual(threadPayload.body);
      expect(thread.date).toEqual(threadPayload.date);
      expect(thread.username).toEqual(threadPayload.username);
    });

    it("should throw an error if the thread doesn't exist", async () => {
      // Assert
      const nonExistentThreadId = 'thread-xxx';
      const threadRepositoryPostgres = new ThreadRepositoryPostgres(pool, {});

      // Action & Assert
      await expect(threadRepositoryPostgres.getThreadById(nonExistentThreadId)).rejects.toThrow(NotFoundError);
    });
  });
});
