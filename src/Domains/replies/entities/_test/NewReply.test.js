const NewReply = require('../NewReply');

describe('a NewReply entity', () => {
  it('should throw error when payload not contain needed property', () => {
    // Arrange
    const payload = {
      content: 'reply content',
    };

    // Action & Assert
    const ERROR_MESSAGE = 'NEW_REPLY.NOT_CONTAIN_NEEDED_PROPERTY';
    expect(() => new NewReply(payload)).toThrowError(ERROR_MESSAGE);
  });

  it('should throw error when payload not meet data type specification', () => {
    // Arrange
    const payload = {
      content: true,
      commentId: 'comment-123',
      owner: 'user-123',
    };

    // Action & Assert
    const ERROR_MESSAGE = 'NEW_REPLY.NOT_MEET_DATA_TYPE_SPECIFICATION';
    expect(() => new NewReply(payload)).toThrowError(ERROR_MESSAGE);
  });

  it('should create NewReply entities correctly', () => {
    // Arrange
    const payload = {
      content: 'reply content',
      commentId: 'comment-123',
      owner: 'user-123',
    };

    // Action
    const newReply = new NewReply(payload);

    // Assert
    expect(newReply).toBeInstanceOf(NewReply);
    expect(newReply.content).toEqual(payload.content);
    expect(newReply.commentId).toEqual(payload.commentId);
    expect(newReply.owner).toEqual(payload.owner);
  });
});
