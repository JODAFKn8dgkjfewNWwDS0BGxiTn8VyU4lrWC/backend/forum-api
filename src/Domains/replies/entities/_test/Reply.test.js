const Reply = require('../Reply');

describe('a Reply entity', () => {
  it('should throw error when payload did not contain needed property', () => {
    // Arrange
    const payload = {
      id: 'reply-123',
      content: 'reply content',
    };

    // Action and Assert
    const ERROR_MESSAGE = 'REPLY.NOT_CONTAIN_NEEDED_PROPERTY';
    expect(() => new Reply(payload)).toThrowError(ERROR_MESSAGE);
  });

  it('should throw error when payload did not meet data type specification', () => {
    // Arrange
    const payload = {
      id: 123,
      username: 'dicoding',
      date: '2024-03-19T09:51:37.522Z',
      content: 'reply content',
      isDelete: false,
    };

    // Action and Assert
    const ERROR_MESSAGE = 'REPLY.NOT_MEET_DATA_TYPE_SPECIFICATION';
    expect(() => new Reply(payload)).toThrowError(ERROR_MESSAGE);
  });

  it('should create Reply object correctly', () => {
    // Arrange
    const payload = {
      id: 'reply-123',
      username: 'dicoding',
      date: new Date(),
      content: 'reply content',
      isDelete: false,
    };

    // Action
    const reply = new Reply(payload);

    // Assert
    expect(reply).toBeInstanceOf(Reply);
    expect(reply.id).toEqual(payload.id);
    expect(reply.username).toEqual(payload.username);
    expect(reply.date).toEqual(payload.date);
    expect(reply.content).toEqual(payload.content);
  });

  it('should create the deleted Reply object correctly', () => {
    // Arrange
    const payload = {
      id: 'reply-123',
      username: 'dicoding',
      date: new Date(),
      content: 'reply content',
      isDelete: true,
    };

    // Action
    const reply = new Reply(payload);

    // Assert
    expect(reply).toBeInstanceOf(Reply);
    expect(reply.id).toEqual(payload.id);
    expect(reply.username).toEqual(payload.username);
    expect(reply.date).toEqual(payload.date);
    expect(reply.content).toEqual('**balasan telah dihapus**');
  });
});
