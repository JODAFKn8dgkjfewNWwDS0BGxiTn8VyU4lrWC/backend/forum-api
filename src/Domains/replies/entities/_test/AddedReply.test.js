const AddedReply = require('../AddedReply');

describe('a AddedReply entity', () => {
  it('should throw error when payload not contain needed property', () => {
    // Arrange
    const payload = {
      id: 'reply-123',
      content: 'reply content',
    };

    // Action & Assert
    const ERROR_MESSAGE = 'ADDED_REPLY.NOT_CONTAIN_NEEDED_PROPERTY';
    expect(() => new AddedReply(payload)).toThrowError(ERROR_MESSAGE);
  });

  it('should throw error when payload not meet data type specification', () => {
    // Arrange
    const payload = {
      id: 123,
      content: 'reply content',
      owner: 123,
    };

    // Action & Assert
    const ERROR_MESSAGE = 'ADDED_REPLY.NOT_MEET_DATA_TYPE_SPECIFICATION';
    expect(() => new AddedReply(payload)).toThrowError(ERROR_MESSAGE);
  });

  it('should create AddedReply entities correctly', () => {
    // Arrange
    const payload = {
      id: 'reply-123',
      content: 'reply content',
      owner: 'user-123',
    };

    // Action
    const addedReply = new AddedReply(payload);

    // Assert
    expect(addedReply).toBeInstanceOf(AddedReply);
    expect(addedReply.id).toEqual(payload.id);
    expect(addedReply.content).toEqual(payload.content);
    expect(addedReply.owner).toEqual(payload.owner);
  });
});
