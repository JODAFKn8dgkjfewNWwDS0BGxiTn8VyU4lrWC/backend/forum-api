const ReplyRepository = require('../ReplyRepository');

describe('ReplyRepository interface', () => {
  it('should throw error when invoke abstract behavior', async () => {
    // Arrange
    const replyRepository = new ReplyRepository();

    // Action & Assert
    const ERROR_MESSAGE = 'REPLY_REPOSITORY.METHOD_NOT_IMPLEMENTED';
    await expect(replyRepository.addReply({})).rejects.toThrowError(ERROR_MESSAGE);
    await expect(replyRepository.verifyReplyOwner('')).rejects.toThrowError(ERROR_MESSAGE);
    await expect(replyRepository.deleteReplyById('')).rejects.toThrowError(ERROR_MESSAGE);
    await expect(replyRepository.getRepliesByThreadId('')).rejects.toThrowError(ERROR_MESSAGE);
  });
});
