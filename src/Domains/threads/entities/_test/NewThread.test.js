const NewThread = require('../NewThread');

describe('a NewThread entity', () => {
  it('should throw error when payload not contain needed property', () => {
    // Arrange
    const payload = {
      title: 'thread title',
      body: 'thread body',
    };

    // Action & Assert
    const ERROR_MESSAGE = 'NEW_THREAD.NOT_CONTAIN_NEEDED_PROPERTY';
    expect(() => new NewThread(payload)).toThrowError(ERROR_MESSAGE);
  });

  it('should throw error when payload not meet data type specification', () => {
    // Arrange
    const payload = {
      title: 'thread title',
      body: 1234,
      owner: true,
    };

    // Action & Assert
    const ERROR_MESSAGE = 'NEW_THREAD.NOT_MEET_DATA_TYPE_SPECIFICATION';
    expect(() => new NewThread(payload)).toThrowError(ERROR_MESSAGE);
  });

  it('should create NewThread entities correctly', () => {
    // Arrange
    const payload = {
      title: 'thread title',
      body: 'thread body',
      owner: 'user-123',
    };

    // Action
    const newThread = new NewThread(payload);

    // Assert
    expect(newThread).toBeInstanceOf(NewThread);
    expect(newThread.title).toEqual(payload.title);
    expect(newThread.body).toEqual(payload.body);
    expect(newThread.owner).toEqual(payload.owner);
  });
});
