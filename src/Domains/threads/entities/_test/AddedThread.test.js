const AddedThread = require('../AddedThread');

describe('a AddedThread entity', () => {
  it('should throw error when payload did not contain needed property', () => {
    // Arrange
    const payload = {
      id: 'thread-123',
      title: 'thread title',
    };

    // Action and Assert
    const ERROR_MESSAGE = 'ADDED_THREAD.NOT_CONTAIN_NEEDED_PROPERTY';
    expect(() => new AddedThread(payload)).toThrowError(ERROR_MESSAGE);
  });

  it('should throw error when payload did not meet data type specification', () => {
    // Arrange
    const payload = {
      id: 'thread-123',
      title: 'thread title',
      owner: 123,
    };

    // Action and Assert
    const ERROR_MESSAGE = 'ADDED_THREAD.NOT_MEET_DATA_TYPE_SPECIFICATION';
    expect(() => new AddedThread(payload)).toThrowError(ERROR_MESSAGE);
  });

  it('should create AddedThread object correctly', () => {
    // Arrange
    const payload = {
      id: 'thread-123',
      title: 'thread title',
      owner: 'user-123',
    };

    // Action
    const addedThread = new AddedThread(payload);

    // Assert
    expect(addedThread).toBeInstanceOf(AddedThread);
    expect(addedThread.id).toEqual(payload.id);
    expect(addedThread.title).toEqual(payload.title);
    expect(addedThread.owner).toEqual(payload.owner);
  });
});
