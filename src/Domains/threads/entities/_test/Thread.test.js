const Thread = require('../Thread');

describe('a Thread entity', () => {
  it('should throw error when payload did not contain needed property', () => {
    // Arrange
    const payload = {
      id: 'thread-123',
      title: 'thread title',
      body: 'thread body',
    };

    // Action and Assert
    const ERROR_MESSAGE = 'THREAD.NOT_CONTAIN_NEEDED_PROPERTY';
    expect(() => new Thread(payload)).toThrowError(ERROR_MESSAGE);
  });

  it('should throw error when payload did not meet data type specification', () => {
    // Arrange
    const payload = {
      id: 123,
      title: 'thread title',
      body: 'thread body',
      date: new Date(),
      username: 'dicoding',
    };

    // Action and Assert
    const ERROR_MESSAGE = 'THREAD.NOT_MEET_DATA_TYPE_SPECIFICATION';
    expect(() => new Thread(payload)).toThrowError(ERROR_MESSAGE);
  });

  it('should create Thread object correctly', () => {
    // Arrange
    const payload = {
      id: 'thread-123',
      title: 'thread title',
      body: 'thread body',
      date: new Date(),
      username: 'dicoding',
    };

    // Action
    const thread = new Thread(payload);

    // Assert
    expect(thread).toBeInstanceOf(Thread);
    expect(thread.id).toEqual(payload.id);
    expect(thread.title).toEqual(payload.title);
    expect(thread.body).toEqual(payload.body);
    expect(thread.date).toEqual(payload.date);
    expect(thread.username).toEqual(payload.username);
  });
});
