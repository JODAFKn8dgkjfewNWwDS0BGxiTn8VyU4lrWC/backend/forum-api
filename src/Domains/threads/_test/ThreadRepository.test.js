const ThreadRepository = require('../ThreadRepository');

describe('ThreadRepository interface', () => {
  it('should throw error when invoke abstract behavior', async () => {
    // Arrange
    const threadRepository = new ThreadRepository();

    // Action & Assert
    const ERROR_MESSAGE = 'THREAD_REPOSITORY.METHOD_NOT_IMPLEMENTED';
    await expect(threadRepository.addThread({})).rejects.toThrowError(ERROR_MESSAGE);
    await expect(threadRepository.getThreadById('')).rejects.toThrowError(ERROR_MESSAGE);
    await expect(threadRepository.verifyThreadExist('')).rejects.toThrowError(ERROR_MESSAGE);
  });
});
