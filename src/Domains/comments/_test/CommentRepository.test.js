const CommentRepository = require('../CommentRepository');

describe('CommentRepository interface', () => {
  it('should throw error when invoke abstract behavior', async () => {
    // Arrange
    const commentRepository = new CommentRepository();

    // Action & Assert
    const ERROR_MESSAGE = 'COMMENT_REPOSITORY.METHOD_NOT_IMPLEMENTED';
    await expect(commentRepository.addComment({})).rejects.toThrowError(ERROR_MESSAGE);
    await expect(commentRepository.verifyCommentOwner('')).rejects.toThrowError(ERROR_MESSAGE);
    await expect(commentRepository.deleteCommentById('')).rejects.toThrowError(ERROR_MESSAGE);
    await expect(commentRepository.getCommentsByThreadId('')).rejects.toThrowError(ERROR_MESSAGE);
    await expect(commentRepository.verifyCommentExist('')).rejects.toThrowError(ERROR_MESSAGE);
  });
});
