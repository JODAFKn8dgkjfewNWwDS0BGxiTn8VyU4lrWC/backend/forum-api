const NewComment = require('../NewComment');

describe('a NewComment entity', () => {
  it('should throw error when payload not contain needed property', () => {
    // Arrange
    const payload = {
      content: 'comment content',
    };

    // Action & Assert
    const ERROR_MESSAGE = 'NEW_COMMENT.NOT_CONTAIN_NEEDED_PROPERTY';
    expect(() => new NewComment(payload)).toThrowError(ERROR_MESSAGE);
  });

  it('should throw error when payload not meet data type specification', () => {
    // Arrange
    const payload = {
      content: true,
      threadId: 'thread-123',
      owner: 'user-123',
    };

    // Action & Assert
    const ERROR_MESSAGE = 'NEW_COMMENT.NOT_MEET_DATA_TYPE_SPECIFICATION';
    expect(() => new NewComment(payload)).toThrowError(ERROR_MESSAGE);
  });

  it('should create NewComment entities correctly', () => {
    // Arrange
    const payload = {
      content: 'comment content',
      threadId: 'thread-123',
      owner: 'user-123',
    };

    // Action
    const newComment = new NewComment(payload);

    // Assert
    expect(newComment).toBeInstanceOf(NewComment);
    expect(newComment.content).toEqual(payload.content);
    expect(newComment.threadId).toEqual(payload.threadId);
    expect(newComment.owner).toEqual(payload.owner);
  });
});
