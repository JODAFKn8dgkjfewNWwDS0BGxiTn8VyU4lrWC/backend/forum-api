const AddedComment = require('../AddedComment');

describe('a AddedComment entity', () => {
  it('should throw error when payload not contain needed property', () => {
    // Arrange
    const payload = {
      id: 'comment-123',
      content: 'comment content',
    };

    // Action & Assert
    const ERROR_MESSAGE = 'ADDED_COMMENT.NOT_CONTAIN_NEEDED_PROPERTY';
    expect(() => new AddedComment(payload)).toThrowError(ERROR_MESSAGE);
  });

  it('should throw error when payload not meet data type specification', () => {
    // Arrange
    const payload = {
      id: 123,
      content: 'comment content',
      owner: 123,
    };

    // Action & Assert
    const ERROR_MESSAGE = 'ADDED_COMMENT.NOT_MEET_DATA_TYPE_SPECIFICATION';
    expect(() => new AddedComment(payload)).toThrowError(ERROR_MESSAGE);
  });

  it('should create AddedComment entities correctly', () => {
    // Arrange
    const payload = {
      id: 'comment-123',
      content: 'comment content',
      owner: 'user-123',
    };

    // Action
    const addedComment = new AddedComment(payload);

    // Assert
    expect(addedComment).toBeInstanceOf(AddedComment);
    expect(addedComment.id).toEqual(payload.id);
    expect(addedComment.content).toEqual(payload.content);
    expect(addedComment.owner).toEqual(payload.owner);
  });
});
