const UserRepository = require('../UserRepository');

describe('UserRepository interface', () => {
  it('should throw error when invoke abstract behavior', async () => {
    // Arrange
    const userRepository = new UserRepository();

    // Action and Assert
    const ERROR_MESSAGE = 'USER_REPOSITORY.METHOD_NOT_IMPLEMENTED';
    await expect(userRepository.addUser({})).rejects.toThrowError(ERROR_MESSAGE);
    await expect(userRepository.verifyAvailableUsername('')).rejects.toThrowError(ERROR_MESSAGE);
    await expect(userRepository.getPasswordByUsername('')).rejects.toThrowError(ERROR_MESSAGE);
    await expect(userRepository.getIdByUsername('')).rejects.toThrowError(ERROR_MESSAGE);
  });
});
