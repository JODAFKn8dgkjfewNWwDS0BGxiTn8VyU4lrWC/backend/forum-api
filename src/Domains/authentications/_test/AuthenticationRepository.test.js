const AuthenticationRepository = require('../AuthenticationRepository');

describe('AuthenticationRepository interface', () => {
  it('should throw error when invoke unimplemented method', async () => {
    // Arrange
    const authenticationRepository = new AuthenticationRepository();

    // Action & Assert
    const ERROR_MESSAGE = 'AUTHENTICATION_REPOSITORY.METHOD_NOT_IMPLEMENTED';
    await expect(authenticationRepository.addToken('')).rejects.toThrowError(ERROR_MESSAGE);
    await expect(authenticationRepository.checkAvailabilityToken('')).rejects.toThrowError(ERROR_MESSAGE);
    await expect(authenticationRepository.deleteToken('')).rejects.toThrowError(ERROR_MESSAGE);
  });
});
